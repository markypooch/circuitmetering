package main

import (
	"Org"
	"Zabbix"
	"os"
	"testing"
)

func TestGetHostID(t *testing.T) {

	//Build our Authentication Request
	//////////////////////////////////////////////////////
	zabbixRequest := Zabbix.ZabbixRequest{
		JsonRPC: "2.0",
		Method:  "user.login",
		Params: map[string]string{
			"user":     os.Getenv("zabbix_username"),
			"password": os.Getenv("zabbix_password"),
		},
		Id:   1,
		Auth: "",
	}

	zabbixAuth := authenticate(os.Getenv("url"), zabbixRequest)

	//Set our hostname, and the information we expect back (hostid, templatename)
	///////////////////////////////////
	zabbixHostFilter := Zabbix.ZabbixFilter{Host: []string{os.Getenv("hostname")}}
	zabbixHostQuery := Zabbix.ZabbixHostQueryRequest{
		Output:                []string{"host", "hostid"},
		SelectParentTemplates: []string{"templateid", "name"},
		Filter:                zabbixHostFilter,
	}

	//Set the method, host.get, our speified query
	////////////////////////////////////
	zabbixRequest.Method = "host.get"
	zabbixRequest.Params = zabbixHostQuery
	zabbixRequest.Auth = zabbixAuth.Result

	zabbixHostQueryResponse := getHostID(os.Getenv("url"), zabbixRequest)

	if len(zabbixHostQueryResponse.Result) == 0 || len(zabbixHostQueryResponse.Result) != 1 {
		t.Errorf("No/Incorrect Number of HostIDs returned; Result Length = %d", len(zabbixHostQueryResponse.Result))
	}

	zabbixHostFilter = Zabbix.ZabbixFilter{Host: []string{"XXXXXXXXXXXXXXXXXXXXX"}}
	zabbixHostQuery = Zabbix.ZabbixHostQueryRequest{
		Output:                []string{"host", "hostid"},
		SelectParentTemplates: []string{"templateid", "name"},
		Filter:                zabbixHostFilter,
	}
	zabbixRequest.Params = zabbixHostQuery

	zabbixHostQueryResponse = getHostID(os.Getenv("url"), zabbixRequest)
	if len(zabbixHostQueryResponse.Result) != 0 {
		t.Errorf("HostIDs returned for invalid hostname; Result Length = %d", len(zabbixHostQueryResponse.Result))
	}
}

func TestGetItems(t *testing.T) {

	//Build our Authentication Request
	//////////////////////////////////////////////////////
	zabbixRequest := Zabbix.ZabbixRequest{
		JsonRPC: "2.0",
		Method:  "user.login",
		Params: map[string]string{
			"user":     os.Getenv("zabbix_username"),
			"password": os.Getenv("zabbix_password"),
		},
		Id:   1,
		Auth: "",
	}

	zabbixAuth := authenticate(os.Getenv("url"), zabbixRequest)

	keyNames := [3]string{"ifAlias", "ifInOctets", "ifOutOctets"}
	zabbixRequest.Auth = zabbixAuth.Result

	for i := 0; i < len(keyNames); i++ {
		zabbixItemContent := getItems(os.Getenv("url"), zabbixRequest, keyNames[i], "10135")
		if len(zabbixItemContent.Result) == 0 {
			t.Errorf("No Items returned for HostID; Result Length = %d", len(zabbixItemContent.Result))
		}
	}

	zabbixItemContent := getItems(os.Getenv("url"), zabbixRequest, "Derp", "10135")
	if len(zabbixItemContent.Result) != 0 {
		t.Errorf("Incorrect number of items returned for invalid key search, should be 0; Result Length = %d", len(zabbixItemContent.Result))
	}

	for i := 0; i < len(keyNames); i++ {
		zabbixItemContent := getItems(os.Getenv("url"), zabbixRequest, keyNames[i], "999999999919")
		if len(zabbixItemContent.Result) != 0 {
			t.Errorf("Incorrect number of items returned for invalid HostID, should be 0; Result Length = %d", len(zabbixItemContent.Result))
		}
	}
}

func TestGetItemHistory(t *testing.T) {

	//Build our Authentication Request
	//////////////////////////////////////////////////////
	zabbixRequest := Zabbix.ZabbixRequest{
		JsonRPC: "2.0",
		Method:  "user.login",
		Params: map[string]string{
			"user":     os.Getenv("zabbix_username"),
			"password": os.Getenv("zabbix_password"),
		},
		Id:   1,
		Auth: "",
	}

	zabbixAuth := authenticate(os.Getenv("url"), zabbixRequest)
	zabbixRequest.Auth = zabbixAuth.Result

	organizations := make(map[string]*Org.Organization)
	organizations["synovia"] = Org.NewOrganization("synovia")
	organizations["synovia"].Ifinterface = Org.IfInterface{
		InterfaceName: "Ethernet1/19.309",
	}
	zabbixItemContents := Zabbix.ZabbixItemContents{
		ItemID:      "429531",
		LastValue:   "0",
		Name:        "ifInOctets Ethernet1/19.309",
		Description: "",
		HostID:      "10135",
	}

	getItemHistory(os.Getenv("url"), zabbixRequest, zabbixItemContents, &organizations, "ifInOctets")
	if len(organizations["synovia"].Ifinterface.IfInBandwidth) != 30 {
		t.Errorf("Incorrect number of Historical datapoints for interface, should be 30; Result Length = %d", len(organizations["synovia"].Ifinterface.IfInBandwidth))
	}

	zabbixItemContents.ItemID = ""
	getItemHistory(os.Getenv("url"), zabbixRequest, zabbixItemContents, &organizations, "ifInOctets")
	if len(organizations["synovia"].Ifinterface.IfInBandwidth) != 0 {
		t.Errorf("Incorrect number of Historical datapoints for interface, should be 0; Result Length = %d", len(organizations["synovia"].Ifinterface.IfInBandwidth))
	}

	zabbixItemContents.Name = ""
	getItemHistory(os.Getenv("url"), zabbixRequest, zabbixItemContents, &organizations, "ifInOctets")
	if len(organizations["synovia"].Ifinterface.IfInBandwidth) != 0 {
		t.Errorf("Incorrect number of Historical datapoints for interface, should be 0; Result Length = %d", len(organizations["synovia"].Ifinterface.IfInBandwidth))
	}
}
