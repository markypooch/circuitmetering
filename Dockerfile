FROM golang:1.12.6-stretch

COPY . /
WORKDIR /

ARG zabbix_username
ARG zabbix_password

RUN export GOPATH="/"
RUN go test -cover
RUN go build DCIParser.go


ENTRYPOINT [ "./DCIParser" ]