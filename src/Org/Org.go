package Org

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type Organization struct {
	OrganizationName string
	Ifinterface      IfInterface
}

type IfInterface struct {
	InterfaceName  string
	IfInBandwidth  []float64
	IfOutBandwidth []float64
}

func NewOrganization(orgName string) *Organization {
	return &Organization{
		OrganizationName: orgName,
	}
}

func (org Organization) DumpOrgToDB(dbHost string, dbName string, dbUsername string, dbPassword string) {
	db, err := sql.Open("mysql", dbUsername+":"+dbPassword+"@tcp("+dbHost+":3306)/"+dbName)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	insert, err := db.Prepare("INSERT INTO vCloud_DCI (org_name, input_bytes, output_bytes, last_update) VALUES (?, ?, ?, ?)")
	if err != nil {
		panic(err)
	}
	defer insert.Close()

	var datetime = time.Now()
	datetime.Format(time.RFC3339)
	for i := 0; i < len(org.Ifinterface.IfInBandwidth); i++ {
		res, err := insert.Exec(org.OrganizationName, org.Ifinterface.IfInBandwidth[i], org.Ifinterface.IfOutBandwidth[i], datetime)
		if err != nil {
			panic(err)
		}
		fmt.Println(res.RowsAffected)
	}
}
