package Zabbix

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

func ZabbixRestCall(url string, request ZabbixRequest) ([]byte, error) {
	var responseBody []byte
	var errorResult error
	jsonEncodedRequest, err := json.Marshal(request)
	if err != nil {
		panic(err)
	}

	httpRequest, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonEncodedRequest))
	if err != nil {
		panic(err)
	}

	httpRequest.Header.Set("Content-type", "application/json-rpc")
	client := &http.Client{}
	response, err := client.Do(httpRequest)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	if response.StatusCode >= 200 && response.StatusCode < 300 {
		responseBody, errorResult = ioutil.ReadAll(response.Body)
	} else {
		errorResult = errors.New("Error")
	}
	return responseBody, errorResult
}
