package Zabbix

type ZabbixRequest struct {
	JsonRPC string      `json:"jsonrpc"`
	Method  string      `json:"method"`
	Params  interface{} `json:"params"`
	Id      int         `json:"id"`
	Auth    string      `json:"auth,omitempty"`
}

type ZabbixAuthResponse struct {
	JsonRPC string `json:"jsonrpc"`
	Result  string `json:"result"`
	Id      int    `json:"id"`
}

type ZabbixFilter struct {
	Host []string `json:"host"`
}
type ZabbixHostQueryRequest struct {
	Output                []string     `json:"output"`
	SelectParentTemplates []string     `json:"selectParentTemplate"`
	Filter                ZabbixFilter `json:"filter"`
}

type ZabbixHostDetails struct {
	HostId string `json:"hostid"`
	Host   string
}
type ZabbixHostQueryResponse struct {
	JsonRPC string              `json:"jsonrpc"`
	Result  []ZabbixHostDetails `json:"result"`
	Id      int                 `json:"id"`
}

type ZabbixItemSearch struct {
	Output     string            `json:"output"`
	Search     map[string]string `json:"search"`
	SortFields string            `json:"sortfield"`
	HostIDs    string            `json:"hostids"`
}

type ZabbixItem struct {
	Result []ZabbixItemContents `json:"result"`
}
type ZabbixItemContents struct {
	ItemID      string `json:"itemid"`
	LastValue   string `json:"lastvalue"`
	Name        string `json:"name"`
	Description string `json:"description"`
	HostID      string `json:"hostid"`
}

type ZabbixHistoryRequest struct {
	Output    string `json:"output"`
	History   int    `json:"history"`
	ItemIDs   string `json:"itemids"`
	SortField string `json:"sortfield"`
	SortOrder string `json:"sortorder"`
	Limit     string `json:"limit"`
}
type ZabbixHistoryContents struct {
	ItemID string `json:"itemid"`
	Clock  string `json:"clock"`
	Value  string `json:"value"`
	Ns     string `json:"ns"`
}
type ZabbixHistoryResponse struct {
	Result []ZabbixHistoryContents `json:"result"`
}
