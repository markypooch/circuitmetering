package main

import (
	"Org"
	"Zabbix"
	"encoding/json"
	"flag"
	"fmt"
	"strconv"
	"strings"
)

func getItemHistory(zabbixUrl string, zabbixRequest Zabbix.ZabbixRequest, item Zabbix.ZabbixItemContents, organizations *map[string]*Org.Organization, keyname string) {

	for key, val := range *organizations {
		fmt.Println(key)
		if strings.Contains(item.Name, val.Ifinterface.InterfaceName) {
			zabbixHistoryRequest := Zabbix.ZabbixHistoryRequest{
				Output:    "extend",
				History:   3,
				ItemIDs:   item.ItemID,
				SortField: "clock",
				SortOrder: "DESC",
				Limit:     "30",
			}

			zabbixRequest.Method = "history.get"
			zabbixRequest.Params = zabbixHistoryRequest

			//Issue our request, and unmarshal the response
			var responseData []byte
			var err error
			responseData, err = Zabbix.ZabbixRestCall("http://"+zabbixUrl+"/zabbix/api_jsonrpc.php", zabbixRequest)
			if err != nil {
				panic(err)
			}
			var zabbixHistoryResponse Zabbix.ZabbixHistoryResponse
			json.Unmarshal(responseData, &zabbixHistoryResponse)

			var values []float64
			for i := 0; i < len(zabbixHistoryResponse.Result); i++ {
				value, err := strconv.ParseFloat(zabbixHistoryResponse.Result[i].Value, 64)
				if err != nil {
					panic(err)
				}

				values = append(values, value/1000000)
			}

			if keyname == "ifInOctets" {
				val.Ifinterface.IfInBandwidth = values
			} else {
				val.Ifinterface.IfOutBandwidth = values
			}

			break
		}
	}
}

func getItems(zabbixUrl string, zabbixRequest Zabbix.ZabbixRequest, keyName string, hostId string) Zabbix.ZabbixItem {

	zabbixItemSearch := Zabbix.ZabbixItemSearch{
		Output: "extend",
		Search: map[string]string{
			"key_": keyName,
		},
		SortFields: "name",
		HostIDs:    hostId,
	}

	zabbixRequest.Method = "item.get"
	zabbixRequest.Params = zabbixItemSearch

	//Issue our request, and unmarshal the response
	var responseData []byte
	var err error

	responseData, err = Zabbix.ZabbixRestCall("http://"+zabbixUrl+"/zabbix/api_jsonrpc.php", zabbixRequest)
	if err != nil {
		panic(err)
	}
	var zabbixItemContent Zabbix.ZabbixItem
	json.Unmarshal(responseData, &zabbixItemContent)

	return zabbixItemContent
}

func authenticate(zabbixUrl string, zabbixRequest Zabbix.ZabbixRequest) Zabbix.ZabbixAuthResponse {

	//Issue our request, and unmarshal the response
	var responseData []byte
	var err error
	responseData, err = Zabbix.ZabbixRestCall("http://"+zabbixUrl+"/zabbix/api_jsonrpc.php", zabbixRequest)
	if err != nil {
		panic(err)
	}
	var zabbixAuth Zabbix.ZabbixAuthResponse
	json.Unmarshal(responseData, &zabbixAuth)

	return zabbixAuth
}

func getHostID(zabbixUrl string, zabbixRequest Zabbix.ZabbixRequest) Zabbix.ZabbixHostQueryResponse {
	//Issue our request, and unmarshal the response
	var responseData []byte
	var err error

	responseData, err = Zabbix.ZabbixRestCall("http://"+zabbixUrl+"/zabbix/api_jsonrpc.php", zabbixRequest)
	if err != nil {
		panic(err)
	}
	var zabbixHostQueryResponse Zabbix.ZabbixHostQueryResponse
	json.Unmarshal(responseData, &zabbixHostQueryResponse)

	return zabbixHostQueryResponse
}

func main() {

	//Import our commandline flags, and derefence them (we don't need them as pointers)
	//////////////////////////////////////////////////////
	zabbixUrl := flag.String("zabbix_url", "", "Url for the target zabbix instance")
	zabbixUsername := flag.String("zabbix_api_username", "", "Username for the API service-account for zabbix")
	zabbixPassword := flag.String("zabbix_api_password", "", "Password for the API service-account for zabbix")
	dbHostName := flag.String("db_hostname", "", "Hostname for the DB to write to")
	dbName := flag.String("db_name", "", "Name of the database to write to")
	dbUsername := flag.String("db_username", "", "Username to establish connection with DB")
	dbPassword := flag.String("db_password", "", "Password to establish connection with DB")
	hostName := flag.String("host_name", "", "Monitored Hostname to search items under")

	flag.Parse()

	//Build our Authentication Request
	//////////////////////////////////////////////////////
	zabbixRequest := Zabbix.ZabbixRequest{
		JsonRPC: "2.0",
		Method:  "user.login",
		Params: map[string]string{
			"user":     *zabbixUsername,
			"password": *zabbixPassword,
		},
		Id:   1,
		Auth: "",
	}
	zabbixAuth := authenticate(*zabbixUrl, zabbixRequest)

	//Set our hostname, and the information we expect back (hostid, templatename)
	///////////////////////////////////
	zabbixHostFilter := Zabbix.ZabbixFilter{Host: []string{*hostName}}
	zabbixHostQuery := Zabbix.ZabbixHostQueryRequest{
		Output:                []string{"host", "hostid"},
		SelectParentTemplates: []string{"templateid", "name"},
		Filter:                zabbixHostFilter,
	}

	//Set the method, host.get, our speified query
	////////////////////////////////////
	zabbixRequest.Method = "host.get"
	zabbixRequest.Params = zabbixHostQuery
	zabbixRequest.Auth = zabbixAuth.Result

	zabbixHostQueryResponse := getHostID(*zabbixUrl, zabbixRequest)

	//Specify the keynames we want to retrieve items
	//for, and create our organization map
	////////////////////////////////////
	keyNames := [3]string{"ifAlias", "ifInOctets", "ifOutOctets"}
	organizations := make(map[string]*Org.Organization)

	//loop through all our keys to search for
	for i := 0; i < len(keyNames); i++ {

		zabbixItemContent := getItems(*zabbixUrl, zabbixRequest, keyNames[i], zabbixHostQueryResponse.Result[0].HostId)
		for j := 0; j < len(zabbixItemContent.Result); j++ {

			switch keyNames[i] {
			case "ifAlias":
				stringContains := "[M-"
				interfaceDescription := zabbixItemContent.Result[j].LastValue

				if strings.Contains(interfaceDescription, stringContains) {

					//retrieve the orgname from the interface description
					intermediaryString := strings.Split(interfaceDescription, "-")[1]
					organizationName := strings.Split(intermediaryString, "]")[0]

					if organizations[organizationName] == nil {
						organizations[organizationName] = Org.NewOrganization(organizationName)
					}
					organizations[organizationName].Ifinterface = Org.IfInterface{
						InterfaceName: strings.Split(zabbixItemContent.Result[j].Name, " ")[1],
					}
				}
				break
			case "ifInOctets":
				fallthrough
			case "ifOutOctets":
				getItemHistory(*zabbixUrl, zabbixRequest, zabbixItemContent.Result[j], &organizations, keyNames[i])
				break
			}
		}
	}

	for k, v := range organizations {
		fmt.Println("Writing metric entry to: " + k)
		v.DumpOrgToDB(*dbHostName, *dbName, *dbUsername, *dbPassword)
	}
}
